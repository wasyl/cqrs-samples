# cqrs-samples


## CQRS Training with Greg
- Design Patterns in .NET -> MediatR
- [x] [Mediator Pattern in .NET](https://github.com/gwasylow/dotnet-design-patterns)
- [x] [Mediator Pattern theory](https://refactoring.guru/design-patterns/mediator)
- [x] [CQRS Sample](https://github.com/gwasylow/dotnet-design-patterns)
- [x] [.NET Core Middleware MSDN](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/record)



# Architecutre of .NET Core WebApps:
![Architecutre of .NET Core WebApps](./Img/webapps.png)


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/wasyl/cqrs-samples.git
git branch -M main
git push -uf origin main
```

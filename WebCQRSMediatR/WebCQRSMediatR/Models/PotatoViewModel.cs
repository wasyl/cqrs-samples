﻿using System;
using System.ComponentModel.DataAnnotations;
using WebCQRSMediatR.Logic.Enums;

namespace WebCQRSMediatR.Models
{
    //ViewModel: Potato
    //TODO: Dodac dodatkowa walidacje dla koloru i rozmiaru
    public class PotatoViewModel
    {
        [Required]
        public String Name { get; set; } 

        [Required]
        public GroceryColor Color { get; set; }

        [Required]
        public GrocerySize Size { get; set; }
        public string Country { get; set; }

        public PotatoViewModel()
        {
        }
    }
}

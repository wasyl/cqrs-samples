﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebCQRSMediatR.Logic.Commands;
using WebCQRSMediatR.Logic.Queries;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;

        public HomeController(ILogger<HomeController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        //Command to the system
        [HttpPost]
        public IActionResult AddPotato(PotatoViewModel potatoViewModel)
        {
            if (ModelState.IsValid)
                return Ok(_mediator.Send(new AddPotatoCommand.Command(potatoViewModel)));

            return NotFound();
        }

        //Query to system
        public IActionResult GetMyPotatos()
        {
            var response = _mediator.Send(new GetMyPotatosQuery.Query());

            //positive validation first
            if (response != null)
                return Ok(response);

            //negative validation at the end
            return NotFound();
        }

        //Second query to the system
        [HttpGet]
        public IActionResult GetMyPotatoByName(string name)
        {
            //initial mvc validation
            if (ModelState.IsValid)
                return Ok(_mediator.Send(new GetMyPotatoByNameQuery.Query(name)));

            //negative validation at the end
            return NotFound();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

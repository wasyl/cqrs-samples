﻿using WebCQRSMediatR.Logic.Queries;

namespace WebCQRSMediatR.Logic.Commands
{
    public class CQRSCommandResponse : CQRSResponse
    {
        public int? ReturnedId { get; set; } = null;
    }
}

﻿using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using WebCQRSMediatR.Logic.Interfaces;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Logic.Commands
{
    public static class AddPotatoCommand
    {
        public class Command : IRequest<CQRSCommandResponse>
        {
            public PotatoViewModel Potato { get; set; }

            public Command(PotatoViewModel potato)
            {
                Potato = potato;
            }
        }

        public class Handler : IRequestHandler<Command, CQRSCommandResponse>
        {
            private readonly IPotatoRepository _potatoRepository;

            public Handler(IPotatoRepository potatoRepository)
            {
                _potatoRepository = potatoRepository;
            }

            public async Task<CQRSCommandResponse> Handle(Command request, CancellationToken cancellationToken)
            {
                if (request.Potato != null)
                {
                    var createdId = _potatoRepository.AddNewPotato(request.Potato);

                    return new CQRSCommandResponse()
                    {
                        ReturnedId = createdId
                    };
                }

                return new CQRSCommandResponse()
                {
                    StatusCode = HttpStatusCode.NotFound,
                    ErrorMessage = "Unable to add potato to repository"
                };
            }
        }
    }
}

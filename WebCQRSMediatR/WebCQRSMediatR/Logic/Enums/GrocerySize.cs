﻿using System;
namespace WebCQRSMediatR.Logic.Enums
{
    public enum GrocerySize
    {
        SMALL = 0,
        MIDDLE,
        LARGE,
        EXTRA_LARGE
    }
}

﻿using System;
namespace WebCQRSMediatR.Logic.Enums
{
    public enum GroceryColor
    {
        YELLOW = 0,
        WHITE,
        RED,
    }
}

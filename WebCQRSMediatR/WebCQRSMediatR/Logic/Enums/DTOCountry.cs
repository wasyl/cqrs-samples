﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCQRSMediatR.Logic.Enums
{
    public enum DTOCountry
    {
        Poland = 0,
        Ukraine,
        Germany
    }

}

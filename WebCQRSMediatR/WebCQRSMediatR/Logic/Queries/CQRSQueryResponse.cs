﻿namespace WebCQRSMediatR.Logic.Queries
{
    //Generic class created to handle all of the queries in system
    public class CQRSQueryResponse<T> : CQRSResponse
    {
        public T QueryResult { get; init; }
    }
}

﻿using System.Net;

namespace WebCQRSMediatR.Logic.Queries
{
    //api readines for of all the queries
    public abstract class CQRSResponse
    {
        public HttpStatusCode StatusCode { get; init; } = HttpStatusCode.OK;
        public string ErrorMessage { get; init; }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using WebCQRSMediatR.Logic.Interfaces;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Logic.Queries
{
    //cqrs query
    //get the information out from the system
    public static class GetMyPotatosQuery
    {
        public class Query : IRequest<CQRSQueryResponse<PotatoResult>>
        {
            public Query()
            {

            }
        }

        //cqrs query handler
        public class Handler : IRequestHandler<Query, CQRSQueryResponse<PotatoResult>>
        {
            private readonly IPotatoRepository _potatoRepository;

            public Handler(IPotatoRepository potatoRepository)
            {
                _potatoRepository = potatoRepository;
            }

            public async Task<CQRSQueryResponse<PotatoResult>> Handle(Query request, CancellationToken cancellationToken)
            {
                var potatos = _potatoRepository.GetAll();
                
                return new CQRSQueryResponse<PotatoResult>()
                {
                    StatusCode = potatos is null ? HttpStatusCode.NotFound : HttpStatusCode.OK,
                    QueryResult = new PotatoResult() { Potatos = potatos },
                    ErrorMessage = potatos is null ? "No potatos found" : string.Empty
                };
            }
        }

        //result of a get my potatos query
        public class PotatoResult
        {
            public IList<PotatoViewModel> Potatos { get; set; }
        }
    }
}

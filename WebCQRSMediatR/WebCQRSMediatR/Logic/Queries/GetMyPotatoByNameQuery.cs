﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using WebCQRSMediatR.Logic.Interfaces;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Logic.Queries
{
    //cqrs query
    //get the information out from the system
    public static class GetMyPotatoByNameQuery
    {
        public class Query : IRequest<CQRSQueryResponse<PotatoResult>>
        {
            public string Name { get; }
            public Query(string name)
            {
                Name = name;
            }
        }

        //cqrs query handler
        public class Handler : IRequestHandler<Query, CQRSQueryResponse<PotatoResult>>
        {
            private readonly IPotatoRepository _potatoRepository;

            public Handler(IPotatoRepository potatoRepository)
            {
                _potatoRepository = potatoRepository;
            }

            public async Task<CQRSQueryResponse<PotatoResult>> Handle(Query request, CancellationToken cancellationToken)
            {
                var potato = _potatoRepository.GetByName(request.Name);

                return new CQRSQueryResponse<PotatoResult>()
                {
                    StatusCode = potato is null ? HttpStatusCode.NotFound : HttpStatusCode.OK,
                    QueryResult = new PotatoResult() { Potato = potato },
                    ErrorMessage = potato is null ? "No potatos found" : string.Empty
                };
            }
        }

        //result of a get my potatos query
        public class PotatoResult
        {
            public PotatoViewModel Potato { get; set; }
        }
    }
}

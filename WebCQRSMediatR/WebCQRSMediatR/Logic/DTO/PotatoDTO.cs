﻿using System;
using WebCQRSMediatR.Logic.Enums;

namespace WebCQRSMediatR.Logic.DTO
{
    public class PotatoDTO
    {
        //NVARCHAR(MAX)
        public String CommercialName;

        //NVARCHAR(128)
        public DTOCountry Country;

        //INT
        public int Color;

        //INT
        public int Size;

        public PotatoDTO()
        {
         
        }
    }
}

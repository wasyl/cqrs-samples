﻿using System.Collections.Generic;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Logic.Interfaces
{
    //TODO: Uzupelnic
    public interface IPotatoRepository
    {
        IList<PotatoViewModel> GetAll();
        PotatoViewModel GetByName(string name);
        int AddNewPotato(PotatoViewModel potatoViewModel);
    }
}

﻿using System;
using WebCQRSMediatR.Logic.DTO;
using WebCQRSMediatR.Logic.Enums;
using WebCQRSMediatR.Models;

namespace WebCQRSMediatR.Logic.Automapper
{
    public static class GreatAutomapper
    {
        private static bool _ignoreCase = true;
        /// <summary>
        /// Map PotatoViewModel properties to PotatoDTO object
        /// </summary>
        /// <param name="potatoDTO"></param>
        /// <returns></returns>
        public static PotatoDTO VMToDTO(PotatoViewModel potatoViewModel)
        {
            if (potatoViewModel != null)
            {
                return new PotatoDTO()
                {
                    Color = (int)potatoViewModel.Color,
                    Country = Enum.Parse<DTOCountry>(potatoViewModel.Country, _ignoreCase),
                    CommercialName = potatoViewModel.Name,
                    Size = (int)potatoViewModel.Size,
                };
            }
            return null;
        }

        /// <summary>
        /// Map PotatoDTO properties to PotatoViewModel object
        /// </summary>
        /// <param name="potatoDTO"></param>
        /// <returns></returns>
        public static PotatoViewModel DTOToVM(PotatoDTO potatoDTO)
        {
            if (potatoDTO != null)
            {
                return new PotatoViewModel()
                {
                    Color = Enum.Parse<GroceryColor>(potatoDTO.Color.ToString(), _ignoreCase),
                    Country = potatoDTO.Country.ToString(),
                    Name = potatoDTO.CommercialName,
                    Size = Enum.Parse<GrocerySize>(potatoDTO.Size.ToString(), _ignoreCase),                    
                };
            }
            return null;
        }
    }
}

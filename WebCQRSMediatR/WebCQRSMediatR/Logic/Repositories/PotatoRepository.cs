﻿using System.Collections.Generic;
using WebCQRSMediatR.Logic.Interfaces;
using WebCQRSMediatR.Models;
using WebCQRSMediatR.Logic.Automapper;
using System.Linq;

namespace WebCQRSMediatR.Logic.Repositories
{
    //TODO: Uzupelnic repository + dodac GreatAutomapper
    //Na wejsciu: ViewModel
    //Na wyjsciu: Dto 
    public class PotatoRepository : IPotatoRepository
    {
        private List<DTO.PotatoDTO> _potatoEntities = new List<DTO.PotatoDTO>();
        public PotatoRepository()
        {
            _potatoEntities.Add(new DTO.PotatoDTO() { CommercialName = "Bryza", Color = 1, Country = Enums.DTOCountry.Poland, Size = 0 });
            _potatoEntities.Add(new DTO.PotatoDTO() { CommercialName = "Irga", Color = 0, Country = Enums.DTOCountry.Ukraine, Size = 2 });
            _potatoEntities.Add(new DTO.PotatoDTO() { CommercialName = "Amerykana", Color = 3, Country = Enums.DTOCountry.Germany, Size = 1 });
            _potatoEntities.Add(new DTO.PotatoDTO() { CommercialName = "Gold", Color = 1, Country = Enums.DTOCountry.Ukraine, Size = 1 });
            _potatoEntities.Add(new DTO.PotatoDTO() { CommercialName = "Gold Premium", Color = 1, Country = Enums.DTOCountry.Ukraine, Size = 2 });
        }

        public int AddNewPotato(PotatoViewModel potatoViewModel)
        {
            var nextId = (_potatoEntities.Count + 1);
            _potatoEntities.Add(GreatAutomapper.VMToDTO(potatoViewModel));
            return nextId;
        }

        public IList<PotatoViewModel> GetAll()
        {
            return _potatoEntities.Select(potato => GreatAutomapper.DTOToVM(potato)).ToList();

            // Odpowiednik do wersji Linq
            //List<PotatoViewModel> lista = new List<PotatoViewModel>();
            //foreach (var item in _potatoEntities)
            //{
            //    lista.Add(GreatAutomapper.DTOToVM(item));
            //}
            //return lista;
        }

        public PotatoViewModel GetByName(string name)
        {
            return GreatAutomapper.DTOToVM(_potatoEntities.FirstOrDefault(potato => potato.CommercialName == name));
        }
    }
}
